from django.db import models

# ferdi 
# Create your models here.
class Status(models.Model):
    name = models.CharField(max_length=135)
    status = models.CharField(max_length=230, blank=True)
    time = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name