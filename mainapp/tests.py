from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import index
from .apps import MainappConfig
from .forms import POSTStatus
from .models import Status


# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/',follow=True)
        self.assertEqual(response.status_code, 200)
        
    def test_using_index(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)

    def test_index_contains_question(self):
        response = Client().get('/home',follow=True)
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, how are you?", response_content)

    def test_using_index_template(self):
        response = Client().get('/home',follow=True)
        self.assertTemplateUsed(response, 'mainapp/index.html')

    def test_apps(self):
        self.assertEqual(MainappConfig.name, 'mainapp')
        self.assertEqual(apps.get_app_config('mainapp').name, 'mainapp')

    def test_model_can_create_new_status(self):
        status = Status.objects.create(status='Status')

        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)
    
    def test_form_validation_for_blank_items(self):
        form = POSTStatus(data={'name': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    
    def test_form_validation_for_filled_items(self) :
        response = self.client.post('', data={'status' : 'Status'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Status')

    # def test_form_name(self) :
    #     response = self.client.post('', data={'name':'alif','status' : 'hi'})
    #     status = Status.objects.get(pk=1)
    #     self.assertEqual(str(status), status.name)
    
    def test_story6_post_success_and_render_the_result(self):
        test = 'Alif'
        response_post = Client().post('', {'name': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/', follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('', {'status_name': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/', follow=True)
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    # def test_valid_Status(self):
    #     status_form = POSTStatus({'status': "status"})
    #     self.assertTrue(status_form.is_valid())
    #     status = Status()
    #     status.status = status_form.cleaned_data['status']
    #     status.save()
    #     self.assertEqual(status.status, "Status")

    def test_invalid_Status(self):
        status_form = POSTStatus({
            'status_name': 301 * "X"
        })
        self.assertFalse(status_form.is_valid())



    