from django import forms
from .models import Status

class POSTStatus(forms.ModelForm):
    class Meta:
        model = Status
        fields = (
            'name',
            'status',
        )
