from django.shortcuts import render, redirect
from .forms import *
from .models import Status
from django.utils import timezone


# Create your views here.
def index(request):
    if request.method == "POST":
        form = POSTStatus(request.POST)
        if form.is_valid():
            status = Status()
            status.name = form.cleaned_data['name']
            status.status = form.cleaned_data['status']
            status.save()
        return redirect('/home')
    else:
        status_count = Status.objects.count()
        form = POSTStatus()
        status = Status.objects.all().order_by('-time')
        response = {'status' : status, 'form': form, 'title':'How r u?','count':status_count}
        return render(request, 'mainapp/index.html', response)
def homeRedirect(request):
    return redirect('/home')
