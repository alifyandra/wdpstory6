# Story 6
This Gitlab repository is the result of work from **Ahmad Izzudin Alifyandra**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/alifyandra/wdpstory6/badges/master/pipeline.svg)](https://gitlab.com/alifyandra/wdpstory6/commits/master)
[![coverage report](https://gitlab.com/alifyandra/wdpstory6/badges/master/coverage.svg)](https://gitlab.com/alifyandra/wdpstory6/commits/master)

## URL
This lab projects can be accessed from [https://how-r-u.herokuapp.com](https://how-r-u.herokuapp.com)

## Author
**Ahmad Izzudin Alifyandra** - [alifyandra](https://gitlab.com/alifyandra)